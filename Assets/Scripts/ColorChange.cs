﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    Color brown = new Color32(139, 69, 19, 1);
    Color skin = new Color32(222, 184, 135, 1);
    GameObject rhino;
    GameObject monkey;
    GameObject snake;

    // Start is called before the first frame update
    void Start()
    {
        rhino = GameObject.Find("Rhino");
        monkey = GameObject.Find("Monkey");
        snake = GameObject.Find("Snake");
        ChangeRhinoColor(Color.gray, Color.grey, Color.grey, Color.grey, Color.black, skin, Color.white);
        ChangeMonkeyColor(brown, skin, brown, skin, skin, Color.black, Color.white);
        ChangeSnakeColor(Color.yellow, Color.green, Color.yellow, Color.green, Color.black, Color.white);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Les paramètres sont dans l'ordre des materials du modèle :
    void ChangeRhinoColor(Color corps, Color ventre, Color machoire, Color tete, Color yeux, Color corne, Color refletsYeux) {
        var materials = rhino.GetComponent<Renderer>().sharedMaterials;

        Debug.Log(materials[0].name);
        materials[0].color = corps;
        Debug.Log(materials[1].name);
        materials[1].color = ventre;
        Debug.Log(materials[2].name);
        materials[2].color = machoire;
        Debug.Log(materials[3].name);
        materials[3].color = tete;
        Debug.Log(materials[4].name);
        materials[4].color = yeux;
        Debug.Log(materials[5].name);
        materials[5].color = corne;
        Debug.Log(materials[6].name);
        materials[6].color = refletsYeux;
    }

    // Les paramètres sont dans l'ordre des materials du modèle :
    void ChangeMonkeyColor(Color corpsEtQueue, Color extremites, Color tete, Color nezEtBouche, Color visage, Color yeux, Color refletsYeux) {
        var materials = monkey.GetComponent<Renderer>().sharedMaterials;

        Debug.Log(materials[0].name);
        materials[0].color = corpsEtQueue;
        Debug.Log(materials[1].name);
        materials[1].color = extremites;
        Debug.Log(materials[2].name);
        materials[2].color = tete;
        Debug.Log(materials[3].name);
        materials[3].color = nezEtBouche;
        Debug.Log(materials[4].name);
        materials[4].color = visage;
        Debug.Log(materials[5].name);
        materials[5].color = yeux;
        Debug.Log(materials[6].name);
        materials[6].color = refletsYeux;
    }

    // Les paramètres sont dans l'ordre des materials du modèle :
    void ChangeSnakeColor(Color boucheEtLangue, Color dos, Color ventre, Color tete, Color yeux, Color refletsYeux) {
        var materials = snake.GetComponent<Renderer>().sharedMaterials;

        Debug.Log(materials[0].name);
        materials[0].color = boucheEtLangue;
        Debug.Log(materials[1].name);
        materials[1].color = dos;
        Debug.Log(materials[2].name);
        materials[2].color = ventre;
        Debug.Log(materials[3].name);
        materials[3].color = tete;
        Debug.Log(materials[4].name);
        materials[4].color = yeux;
        Debug.Log(materials[5].name);
        materials[5].color = refletsYeux;
    }
}
